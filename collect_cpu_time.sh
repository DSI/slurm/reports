#!/usr/bin/env bash

odir="./cpu_times"

verbose=""
while getopts "hs:e:o:v" opt
do
    case $opt in
	e)
	    edate=$OPTARG
	    ;;
	s)
	    sdate=$OPTARG
	    ;;
	h)
	    echo "Usage: $0 -s <start data> [-e <end date>] [-h] [-o <ouput dir> (default ./cpu_times)]."
	    exit 1
	    ;;
	v)
	    verbose=1
	    ;;
	o)
	    odir=$OPTARG
	    ;;
	?)
	    echo "Unknow option $opt" >2
	    ;;
    esac
done

if [ -z "$sdate" ]
then
    echo "Missing mandatory -s <start date> option." >&2
    exit -1
fi

function get_cpu_time() {
    account=$1
    if [ -z "$edate" ]
    then 
	cpu_time=$((echo 0; sacct -n -X -S $sdate           -o CPUTimeRAW -A $account) | paste -sd+ - | bc)
    else 
	cpu_time=$((echo 0; sacct -n -X -S $sdate -E $edate -o CPUTimeRAW -A $account) | paste -sd+ - | bc)
    fi
    echo $cpu_time
}

# Labs are directly under the root account
# There is a documented bug in sacct that will print all accounts whatever the filter
# That is, Parent=root only main concequence is that the parent name will only be printed
# if it's root and left banck otherwise.
labs=$(sacctmgr show account WithAssoc format=Account,ParentName Parent=root -P -n | egrep -v "\|$" | sed -e 's/|root//')

[ -z "$verbose" ] || (echo -n "Found labs: "; for l in $labs ; do echo -n "$l "; done; echo)

rm -rf $odir
mkdir $odir

(
    echo -n "from $(date -d  $(echo $sdate | sed -e 's/\./\//g') +"%d %B %Y")"
    if [ -z "$edate" ]
    then
	echo
    else 
	echo -n " to $(date -d  $(echo $edate | sed -e 's/\./\//g') +"%d %B %Y")"
    fi
) > $odir/_period

for lab in $labs
do
    [ -z "$verbose" ] || echo "Processing lab $lab."
    mkdir $odir/$lab
    lab_cpu_time=0
    teams=$(sacctmgr show account WithAssoc format=Account,ParentName Parent=$lab -P -n | egrep -v "\|$" | sed -e "s/|$lab//")
    for team in $teams
    do
	[ -z "$verbose" ] || echo -n "Processing team $team."
	cpu_time=$(get_cpu_time $team)
	[ -z "$verbose" ] || echo " -> $cpu_time s."
	let lab_cpu_time+=$cpu_time
	echo "$team $cpu_time" >> $odir/$lab/teams
    done
    [ -z "$verbose" ] || echo -n "Unasigned to a team."
    cpu_time=$(get_cpu_time $lab)
    echo "<other> $cpu_time" >> $odir/$lab/teams
    sort -k2 -n -r $odir/$lab/teams > $odir/$lab/teams.sorted
    let lab_cpu_time+=$cpu_time
    [ -z "$verbose" ] || echo " -> $cpu_time s."
    echo $lab_cpu_time > $odir/$lab/_total
done

[ -z "$verbose" ] || echo "Results in $odir."

