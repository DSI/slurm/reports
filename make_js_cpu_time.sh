#!/usr/bin/env bash

idir="./cpu_times"

verbose=""
while getopts "hi:vo:" opt
do
    case $opt in
	h)
	    echo "Usage: $0 -i <cpu_time dir> [-h] [-v]."
	    exit 1
	    ;;
	v)
	    verbose=1
	    ;;
	i)
	    idir=$OPTARG
	    ;;	
	?)
	    echo "Unknow option $opt" >2
	    ;;
    esac
done

if [ -z "$idir" ]
then
    echo "Missing -i <time dir>"  >&2 
    exit -1
fi

echo  "var timeRange = '$(cat $idir/_period)';"
echo  "var colors = Highcharts.getOptions().colors;"
units=$(ls $idir | egrep -v "^_" | sort)
echo "var units = ["
for unit in $units
do
    echo "   '$unit',"
done
echo "];"
echo "var data = ["
coloridx=1
for unit in $units
do
    
    echo "    {"
    echo "       y: $(cat $idir/$unit/_total),"
    echo "       color: colors[$coloridx],"
    let coloridx++
    echo "       drilldown: {"
    echo "          name: '$unit',"
    echo "          teams: ["
    teams=$(cat $idir/$unit/teams.sorted | awk '{print $1;}')
    for team in $teams
    do
	echo "            '$team',"  | sed -e 's/<other>/others/'
    done
    echo "          ],"
    echo "          data : ["
    cpu_times=$(cat $idir/$unit/teams.sorted | awk '{print $2;}')
    for cpu_time in $cpu_times
    do
	let hours=cpu_time/60/60
	echo "            $hours,"
    done
    echo "          ]"
    echo "       }"
    echo "    },"
done
echo "];"

