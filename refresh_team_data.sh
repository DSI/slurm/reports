#!/usr/bin/env bash

# must be run as root
here=$(dirname $0)
$here/collect_cpu_time.sh -s 06.01.19 -o /tmp/since-06.01.19
$here/make_js_cpu_time.sh -i /tmp/since-06.01.19 > /tmp/teams_data.js
scp /tmp/teams_data.js root@camembert-n.oca.eu:/srv/www-stats-licallo/
rm /tmp/teams_data.js /tmp/since-06.01.19
